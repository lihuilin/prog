OBJS=main.o explorer.o premier.o my_system.o
CC=gcc

Prog_premiers : $(OBJS)
$(CC) $(OBJS) -o $@
main.o : main.c explorer.h
$(CC) -c $*.c
explorer.o : explorer.c my_system.h explorer.h premier.h
$(CC) -c $*.c
premier.o : premier.c premier.h
$(CC) -c $*.c
my_system.o : my_system.c my_system.h
$(CC) -c $*.c
clean :
rm $(OBJS) Prog_premiers
