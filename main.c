#include<unistd.h>
#include<stdio.h>
#include<stdlib.h>
#include<sys/wait.h>
#include<string.h>
#include<sys/types.h>
int premier(int nb)
{
    int r=0;

    for(int i = 1 ; i <= nb ; i++ )
    {
        if(nb % i == 0)
        {
           r++;
        }
    }

    if(r>2)
       return 0;
    else
       return 1;

}
void my_system(char*path,char**argv)
{
    int pid=fork();
    int etat;
    if(pid==0)
    {
        execv(path,argv);
    }
    else{
        waitpid(pid,&etat,0);
    }
}
void explorer(int debut, int fin){
  int etat,pid,pid2;
  pid=fork();//子进程完全继承父进程 他们分别返回两个值，通过判断分别操作
  if(pid==0){
    for (int i=debut; i<=fin;i++){

      if (premier(i)==1) {
        pid2=fork();
        fflush(stdout);
        if (pid2==0){
          char chaine[100];
          sprintf(chaine,"echo ' %d  est un nombre premier \
 le pid du processus qui a cree le processus system est %d,celui de son pere:%d\n",i,getpid(),getppid());
         system(chaine);//三步走 执行这个字符串命令：fork一个子程序 调用execv 父进程等待子进程
          sleep(2);//停下来 不做 不执行
          exit(0);// 退出 并发送0信号
        }
        else wait(&etat);// instruction 41//等待子进程结束并存它的结束信号到etat
      }

    }
    exit(0);

  } else wait(&etat);// instruction 46

}
int main(){
  int grp=1;
  while(grp<=11){
    explorer(grp+1,grp+10);
    grp=grp+10;

  }
}
