void my_system(char*path,char**argv)
{
    int pid=fork();
    int etat;
    if(pid==0)
    {
        execv(path,argv);
    }
    else{
        waitpid(pid,&etat,0);
    }
}
